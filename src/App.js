import React, { Component } from 'react';
import logo from './logo.svg';
import { createRoulette } from './tools/createRoulette'
import { roll } from './tools/roll'
import { DisplayRoulette } from './presentational/DisplayRoulette'
import { DisplayRandomNumber } from './presentational/DisplayRandomNumber'
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      id:0,
      roulettes: [],
      randomNumber: null,
    }
  }

  addRoulette = () => {
    const roulette = createRoulette();
    const id = this.state.id;
    const uniqueRoulette = {[id]: roulette};
    const newRoulettes = this.state.roulettes.concat(uniqueRoulette);
    this.setState({ 
      id: id+1,
      roulettes: newRoulettes
    })
  }

  componentDidMount() {
    this.addRoulette(); 
  }

  handleRoll = () => {
    const randomNumber = roll(); // funkcja z losowaniem od 0 do 36 liczby
    this.setState({ randomNumber }) // to jest tez 'magiczny' zapis, to tak jakbym napisal { randomNumber: randomNumber }
  }

  // jako ze chce walnac jakas ladna wiadomosc gdy jeszcze ktos nie losowal liczby, to zrobie se do tego funkcje

  render() {
    // a tak btw wszystkie stringi oplatam w{``} znaczek, a to dlatego ze wtedy moge nawet emoji do srodka wjebac i sie ladnie zrenderuje
    const { roulettes, randomNumber } = this.state // w tej linijce 'zgarniam' wartosci ze state'a (zwyklego obiektu tam na gorze o ) magicznym sposobem jsa, zwanym object destructuring
    console.log(roulettes)
    return (
      <div className="App">
        <header className="App-header">
          { // podaje swojej funkcji (stateless componentowi, wyzsza szkola jazdy xd) parametr ze state zeby ladnie pykalo
            <DisplayRandomNumber
              randomNumber={randomNumber}
            />
          }
          <p>
            <button onClick={this.addRoulette}>{`Create roulette`}</button>
            <button onClick={this.handleRoll}>{`Roll the number !`} </button> 
            Edit <code>src/App.js</code> and save to reload.
          </p>
          {this.state.roulettes.length > 0 ? roulettes.map(roulette => {
            const id = Object.keys(roulette)[0] // accessing property in a stupid way because why not performance is for pussies
            return <DisplayRoulette
                      id={id}
                      roulette={roulette[id]}
                    />
          }) : null}
        </header>
      </div>
    );
  }
}

export default App;

// render to jest cos takiego z reacta generalnie, mozna to tak nazwac troche ze odpala sie 'kiedy potrzebuje', tam sa algorytmy decydujace czy potrzebuje sie odswiezyc komponent czy ninvoke
// a to sie fajnie odswieza, bo sa te service Workery o ktorych wspomniales wczesniej, ktore tak naprawde sa tymi, no , nie pamietam nazwy
// socketami takimi, o
// i patrza -> jakas zmiana w kodzie ? render
// ez 