import React from 'react';

export const DisplayRoulette = (props) => {
    const { id, roulette } = props; // znowu object destructuring, czyli wiem ze podam mu { id. roulette } jako 'prop' (czyli zmienne uformowane w object tak jakby) 
    // i podajac ta sama nazwe, moge przyjac ich wartosci za pomoca tego smiesznego syntaxu
    console.log(id, roulette)
    const divisions = Object.entries(roulette);
    const displayedDivisions = divisions.map(division => {
        const [number, color] = division;
        return DisplayDivision(number, color, id)
    })
    return (
        <div> {displayedDivisions} </div> // generalnie to cos gdzies zjebalem, bo zamysl jest taki w reactie, ze jak masz kilka elemtnow takich samych, tak jak np. te z 
        // funkcji DisplayDivision, to trzeba je jakos odrozniac. zeby to robic nadaje sie unikatowe klucze elementom
        // ja gdzie zapomnialem dac klucza lae nie chce mi sie szukac gdzie xd
        // a, jak zapomnisz dac klucza to moze sie popieprzyc wszystko w appce, totalnie losowe rzeczy, a jest szansa ze tak nie bedzie
        // deal with it
        // nie no sledzi sie, pokaze Ci
        // lol nope jednak nie pokaze, w incognito nie mam raect dev tooli 
    )
}

const DisplayDivision = (number, color, id) => <div key={`${number}-${color}-${id}`} style={{ height: 50, width: 50, backgroundColor: color }}> {number} </div>