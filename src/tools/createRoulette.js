const createBlackDivisions = (fieldsAmount = 36, startingPoint = 2) => {
    let blackDivisions = {};
    for(let i=startingPoint; i<fieldsAmount; i+=2) {
        blackDivisions[i] = 'black'
    }
    return blackDivisions;
}

const createWhiteDivisions = (fieldsAmount = 36, startingPoint = 1) => {
    let whiteDivisions = {};
    for(let i=startingPoint; i<fieldsAmount; i+=2) {
        whiteDivisions[i] = 'red'
    }
    return whiteDivisions;
}

const createGreenDivision = () => {
    return { 0: 'green' }
}

export const createRoulette = (fieldsAmount = 36, startingPoint = 1) => {
    const blackDivisions = createBlackDivisions(fieldsAmount, startingPoint+1);
    const whiteDivisions = createWhiteDivisions(fieldsAmount, startingPoint);
    const greenDivisions = createGreenDivision();

    const roulette = {...blackDivisions, ...whiteDivisions, ...greenDivisions};
    return roulette
}